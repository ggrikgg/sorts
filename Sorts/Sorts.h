#pragma once

#include <vector>
#include <iostream>
template<typename _iterator>
void selectionSort(_iterator first, _iterator last)
{
	_iterator j;
		auto tmp=*first;
	for (auto i = first; i<last; i++) 
	{
		j = i;
		for (auto k = i+1; k<last; k++)
		{
			if (*k<*j) {
				j = k;
			}
		}
		tmp = *i;
		*i = *j;
		*j = tmp;
	}
}

template<typename _iterator>
void bubbleSort(_iterator first, _iterator last)
{
	auto tmp = *first;
	for (auto i = first; i<last; i++) 
	{
		for (auto j = i + 1; j<last; j++)
		{
			if (*j<*(j - 1))
			{
				tmp = *j;
				*j = *(j - 1);
				*(j - 1) = tmp;
			}
		}
	}
}
template<typename _iterator>

void insertionSort(_iterator first, _iterator last)
{
	auto tmp = *first;
	auto i = first;
	for (auto j = first+1; j<last; j++)
	{
		tmp = *j;
		i = j - 1;
		while (i >= first && *i>tmp) 
		{
			*(i + 1) = *i;
			i = i - 1;
			*(i + 1) = tmp;
		}
	}
}


typedef std::ptrdiff_t Index;

template <typename _Iterator>
void HoareQuickSort(_Iterator a, _Iterator finish)
{
	Index  j = (finish - a) - 1;
	Index i = 0;

	if (j <= i)
		return;

	auto pivot = a[i + (j - i) / 2];

	while (i <= j)
	{
		while (a[i]< pivot)
			++i;

		while (pivot< a[j])
			--j;

		if (i <= j)
		{
			std::swap(a[i++], a[j--]);
		}
	}

	HoareQuickSort(a, a + (j + 1));
	HoareQuickSort(a + i, finish);
}



template<typename _iterator, typename T>
void merge_(_iterator first1, _iterator last1, _iterator first2, _iterator last2, const T& pv)
{
	const int vecsize = (last2 - first1);
	T* temp= new T[vecsize];
	Index i=0, j=0;
	Index i1 = last1-first1-1, j1 = last2-first2-1;
	size_t k = 0;

	while (i <= i1 && j <= j1)
	{
		if (first1[i] < first1[j])
		{
			temp[k++] = first1[i];
			i++;
		}
		else
		{
			temp[k++] = first1[j];
			j++;
		}
	}

	while (i <= i1)
	{
		temp[k++] = first1[i];
		++i;
	}
	while (j <= j1)
	{
		temp[k++] = first1[j];
		j++;
	}
	size_t z;
	for (i = 0, z= 0; i <= j1; i++, z++)
		first1[i] = temp[z];
}


template<typename _iterator>
void mergesort_(_iterator first, _iterator last)
{
	_iterator mid;

	if (first<last)
	{ 
		mid = (first + ((last - first) / 2));
		mergesort_(first, mid);
		mergesort_(mid + 1, last);
		merge_(first, mid, mid + 1, last,*first);
	}
}

template<typename _iterator, typename T>
size_t binsearch(T elem,_iterator first, _iterator last)
{
	_iterator mid;

	while (first<last)
	{
		mid = (first + ((last - first) / 2));
		if (elem == *mid)
			return mid - first+1;
		if (elem > (*mid))
		{
			first = mid + 1;
		}
		else
		{
			last = mid;
		}
	}
	return 0;
}









